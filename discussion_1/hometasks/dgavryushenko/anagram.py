from sys import argv

filename = argv[1]
vvod = input('Введите слово: ')

sorted_vvod = sorted(vvod)

list_anagram = []

with open(filename, 'r') as f:
    result = (f.read()).split(' ')
    for word in result:
        if len(sorted_vvod) == len(word):
            if sorted_vvod == sorted(word) and vvod != word and word not in list_anagram:
                list_anagram.append(word)

print('По слову {} найдено {} анаграмм(а)'.format(vvod, len(list_anagram)))

print(list_anagram)
