# -*- coding: utf-8 -*-

class MyLogger:

	__log_filename = " "
	__counter = 1

	def __init__(self):
		self.__id = MyLogger.__counter
		MyLogger.__counter += 1

	def set_log_filename(filename):
		MyLogger.__log_filename = filename

	def log(self, message):
		with open(MyLogger.__log_filename, "a") as f:
			f.write("{}\t{}\n".format(self.__id, message))


def main():
	MyLogger.set_log_filename("fuu.txt")

	logger = MyLogger()
	logger.log("OLOLOLOL")

	logger2 = MyLogger()
	logger2.log("ROFL")

	print("OK")


if __name__ == '__main__':
	main()
