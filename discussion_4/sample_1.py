# -*- coding: utf-8 -*-

from collections import Counter
from collections import defaultdict 
from collections import OrderedDict 
from collections import deque  
from collections import ChainMap
from collections import namedtuple


def counter_sample():
	my_list = [1, 2, 3, 4, 1, 2, 6, 7, 3, 8, 1]  
	cnt = Counter(my_list)
	print(cnt) 
	print(list(cnt.elements()))

	print("################")
	
	other_cnt = Counter({1:5, 2:2, 8:1})
	print(other_cnt)
	# print(list(other_cnt.elements())) 
	# cnt.subtract(other_cnt)
	# print(cnt)
	print(lol)


def most_common_word():
	text = "hello say hello to my little friend, \
		 then say hello to yourself and all."
	words = text.split()
	cnt = Counter(words)
	print(cnt.most_common())


def defaultdict_sample():
	nums = defaultdict(int)  
	nums['one'] = 1  
	nums['two'] = 2  
	print(nums['three'])  

	count = defaultdict(int)  
	names_list = "Mike John Mike Anna Mike John John Mike Mike Britney Smith Anna Smith".split()  
	for names in names_list:  
		count[names] +=1
	print(count)  


def ordered_dict_sample():
	od = OrderedDict()  
	od['b'] = 2  
	od['a'] = 1  
	od['c'] = 3  
	print(od)  

	for key, value in od.items():  
		print(key, value)


def dict_with_counter():
	text = "hello Say hello to my little friend, then say hello to yourself and all"
	words = text.split()
	cnt = Counter(words)
	od = OrderedDict(cnt.most_common())  

	for key, value in od.items():  
		print(key, value)


def deque_sample():
	# my_list = ["a","b","c"]  
	deq = deque(["a","b","c"])  
	print(deq)  

	# deq.append("d")  
	# deq.appendleft("e")  
	# print(deq)  

	# deq.pop()  
	# deq.popleft()  
	# print(deq)  

	# deq.appendleft("a")
	# deq.appendleft("a")
	# print(deq) 
	# print(deq.count("a"))  


def chainmap_sample():
	dict1 = { 'a' : 1, 'b' : 2 }  
	dict2 = { 'c' : 3, 'b' : 4 }  
	chain_map = ChainMap(dict1, dict2) 
	dict1["eee"] = 666
	print(chain_map.maps)  

	print(chain_map['a'])
	print(chain_map['b'])

	dict2['c'] = 5  
	print(chain_map.maps)  

	print (list(chain_map.keys()))  
	print (list(chain_map.values()))  

	new_chain_map = chain_map.new_child({'e' : 5, 'f' : 6})  
	print(new_chain_map) 
	print(new_chain_map.maps[0])


def named_tuple_sample():
	# TesterTuple = namedtuple('Tester', 'fio, age, salary')
	# tester1 = TesterTuple('Fedya', 22, 10)
	# tester2 = TesterTuple('Zhora', 24, 13)
	# # print(tester1)
	# # print(tester1.fio)

	# list_of_testers = []
	# list_of_testers.append(tester1)
	# list_of_testers.append(tester2)

	# print(list_of_testers)
	# for i in list_of_testers:
	# 	print(i.age)

	# tester3 = TesterTuple._make(['Vasya', 33, 15])
	# print(tester3)

	# tester_data = tester1._asdict()
	# print(tester_data)

	# # tester1 = tester1._replace(salary=11)
	# print(tester1)

	dsdsd = {222, 666, '2222', 222, 222}
	dsdsd.add(536346)
	print(dsdsd)

	lol = (1, 'asdfasfa', 3444)
	print(lol[1])

def main():
	# counter_sample()
	# most_common_word()
	# defaultdict_sample()
	# ordered_dict_sample()
	# dict_with_counter()
	# deque_sample()	
	# chainmap_sample()
	named_tuple_sample()


if __name__ == "__main__":
	main()
