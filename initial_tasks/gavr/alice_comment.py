from sys import argv
import re

'''Для передачи текстового файла, в качестве аргумента'''
filename = argv[1] 

'''Создается отдельная переменная, в которую собирается регулярное выражение'''
select = re.compile("([a-zA-Z]+)")

'''Создается словарь, в который будут заноситься слова и кол-во повторений'''
slovar = {}

'''Все слова, попавшие под фильтр, заносятся в список result''' 
with open(filename, 'r') as f:   
     result = select.findall(f.read())

'''Цикл проходится по каждому элементу из списка'''
for word in result:
    word = word.lower() #Слово записывается с нижним регистром
    if word in slovar:
        number = slovar[word]
        slovar[word] = number + 1 #Если слово уже есть в словаре, то к его значению добавляется единица
    else:
        slovar[word] = 1 #Иначе значение для этого слова будет равно единице

'''Здесь словарь сортируется по значению каждого ключа (то есть сколько раз оно повторилось) и реверсируется (от большего к меньшему)'''
sorted_word = sorted(slovar, key=lambda x: int(slovar[x]), reverse=True)

'''Открывается файл в режиме записи (если его нет, то создается)'''
with open('output.txt', 'w') as output:
    for word in sorted_word: #проходимся по каждому элементу отсортированного списка
        word_quant = str("{0}:{1}\n").format(word, slovar[word]) #записываем элемент и кол-во его повторений
        output.write(word_quant) #результат записывается в файл

num_words = int(input('Сколько самых повторяемых слов вывести? '))

print ('Самые повторяющиеся слова: ' + '\n', (sorted_word[0:num_words]))
