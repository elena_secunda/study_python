def count_words(wordlist,word):
    count = 0
    for w in wordlist:
        if w==word:
            count+=1
    return count

def sort_string_keys_in_dict (dictionary):
    outdictionary = {}
    for key in sorted(dictionary.keys()) :
        
        outdictionary[key] = dictionary[key]

    return outdictionary

def sort_int_value_in_dict (dictionary):
    outdictionary = {}
    outdictionary = sorted(dictionary.items(), key=lambda x: x[1])
    return dict(outdictionary)



removefromtext = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		  ' II.',' III.',' IV.',' V.',' VI.',' VII.',' VIII.',' IX.',' X.',' XI.',' XII.',
		  " ' ", "\n'","''", "'\n", '\n', " '", "' ",
		  ".'", ",'", "?'", "!'",
		  ';', ':', '--', '(', ')', '[', ']',
                  '. ', '*', ', ',
                  '.', '! ', '? ', '_',
                  '!', '?', ',', '"'] 
wordlist = []
alice_dictionary = {}
alice_dictionary_sorted = {}

with open("Alice.txt") as textfile:
    text = textfile.read()
    #убрать возможные знаки препиинания и цифры - римские тоже(так как задание считать именно слова)
    #(тут конечно не предусмотреть все возможные варианты, но такой вариант плюс минус работает)
    for symbol in removefromtext:
            text = text.replace(symbol," ") 

    #убрать оставшиеся одинарные кавычки в начале и конце слов - из-за одного неприятного места в тексте, где подряд идут '' и ""
    for symbol in removefromtext[26:28]:
            text = text.replace(symbol,"") 


    #разбить весь текст на список по признаку, что между словами пробелы
    text = text.split(' ')
    for word in text:

        #Всё кроме 'I' делаем нижним регистром, чтобы одни и те же слова не считать за разные.
        #Это, конечно, знатная недоработка(( так как по-хорошему нужно различать имена собственные и нет.
        #Но я посчитала для простоты - пусть лучше я потеряю информацию об именах собственных, чем The и the, например,
        #буду считать разными словами,
        #а еще исключаем из списка пробелы, так как они после replace остались в некоторых местах
        if word != "I" and word != ' ' :
            word = word.lower() 
            wordlist.append(word)
        elif word == "I":
            wordlist.append(word)

#приведение в соответствие слов и их количества в словаре
for w in wordlist:
    alice_dictionary[w] = count_words(wordlist,w)


#cортировка по алфавиту (по заданию не требовалась, но глазу приятней)
alice_dictionary = sort_string_keys_in_dict(alice_dictionary)

#запись словаря в файл
outfile = open("alice_dictionary.txt", "w")
for k, v in alice_dictionary.items():
    outfile.write(str(k) + ' - '+ str(v) + '\n')



#ввод числа - сколько самых часто встречающихся слов записать в вфайл?
print('Введите, сколько слов писать в файл')
N = int(input())

#сортировка по величине
alice_dictionary_sorted = sort_int_value_in_dict(alice_dictionary)

#запись нужнгого числа самых часто встречающихся слов в файл
#так как отсортировано по возрастанию, а не по убыванию - идем от последнего индекса с шагом -1
outfile2 = open("alice_dictionary_N_most_freq.txt", "w")
last_index = len(alice_dictionary_sorted)
first_index = len(alice_dictionary_sorted)-(int(N)+1)
words_sorted_by_freq = (list(alice_dictionary_sorted.keys()))

#обрезать то, что не вошло в требуемый диапазон
words_sorted_by_freq= words_sorted_by_freq [last_index : first_index : -1]

#запись в файл
for word in words_sorted_by_freq:
    outfile2.write(str(word) + '\n')




textfile.close()
outfile.close()
outfile2.close()
